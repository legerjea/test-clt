use itertools::Itertools;

struct DiscreteIntegerPDistrib {
    mean: f64,
    var: f64,
    pmf: Vec<f64>,
}

impl DiscreteIntegerPDistrib {
    fn zero() -> Self {
        Self {
            mean: 0f64,
            var: 0f64,
            pmf: vec![1.],
        }
    }
    fn add_indep_bernoulli(&self, p: f64) -> Self {
        let q = 1f64 - p;
        let new_mean = self.mean + p;
        let new_var = self.var + p * q;
        let prob0 = self.pmf.iter().chain(std::iter::once(&0f64));
        let prob1 = std::iter::once(&0f64).chain(self.pmf.iter());
        let new_pmf: Vec<f64> = prob1.zip(prob0).map(|(&a, &b)| a * p + b * q).collect();

        Self {
            mean: new_mean,
            var: new_var,
            pmf: new_pmf,
        }
    }

    fn _check_constitency(&self) -> bool {
        let mass: f64 = self.pmf.iter().sum();
        let mean: f64 = self
            .pmf
            .iter()
            .enumerate()
            .map(|(i, &p)| (i as f64) * p)
            .sum();
        let var: f64 = self
            .pmf
            .iter()
            .enumerate()
            .map(|(i, &p)| ((i as f64) - mean).powi(2) * p)
            .sum();

        (mass - 1f64).abs() < 1e-12
            && (mean - self.mean).abs() < 1e-12
            && (var - self.var).abs() < 1e-12
    }

    fn calc_cdf(&self) -> Vec<f64> {
        self.pmf
            .iter()
            .scan(0f64, |cum, &p| {
                *cum += p;
                Some(*cum)
            })
            .collect()
    }

    fn norm_cdf_dinf(&self) -> f64 {
        let std2: f64 = (self.var * 2f64).sqrt();
        let cdf = self.calc_cdf();

        self.pmf
            .iter()
            .enumerate()
            .map(|(i, _)| {
                (statrs::function::erf::erf(((i as f64) - self.mean) / std2) + 1f64) / 2f64
            })
            .zip(std::iter::once(&0f64).chain(cdf.iter()))
            .zip(cdf.iter())
            .map(|((ncdf, &cdf1), &cdf2)| f64::max((ncdf - cdf1).abs(), (ncdf - cdf2).abs()))
            .reduce(f64::max)
            .unwrap_or_else(|| -1. / 0.)
    }

    fn norm_discrete_pmf_ninf_relative(&self) -> f64 {
        let std2: f64 = (self.var * 2f64).sqrt();

        (self
            .pmf
            .iter()
            .chain(std::iter::once(&0f64))
            .enumerate()
            .map(|(i, _)| {
                (statrs::function::erf::erf(((i as f64) - 0.5 - self.mean) / std2) + 1f64) / 2f64
            })
            .tuple_windows()
            .map(|(a, b)| b - a)
            .zip_eq(self.pmf.iter())
            .map(|(a, b)| (a - b).abs())
            .reduce(f64::max)
            .unwrap())
            / self.pmf.iter().map(|&x| x).reduce(f64::max).unwrap()
    }
}

struct BernoulliSum<F> {
    n: u64,
    state: DiscreteIntegerPDistrib,
    prob_fun: F,
}

impl<F> BernoulliSum<F> {
    fn new(prob_fun: F) -> Self {
        Self {
            n: 1,
            state: DiscreteIntegerPDistrib::zero(),
            prob_fun: prob_fun,
        }
    }
}

impl<F> Iterator for BernoulliSum<F>
where
    F: Fn(u64) -> f64,
{
    type Item = DiscreteIntegerPDistrib;

    fn next(&mut self) -> Option<Self::Item> {
        let prob = &(self.prob_fun)(self.n);
        let mut state = self.state.add_indep_bernoulli(*prob);
        std::mem::swap(&mut state, &mut self.state);
        self.n += 1;

        Some(state)
    }
}

fn main() {
    let mut args = std::env::args();
    args.next();
    let limit: usize = match args.next() {
        Some(value) => value.parse().unwrap(),
        None => 100,
    };
    let bsum_type = match args.next() {
        Some(value) => value,
        None => String::from("binomial(0.5)"),
    };
    let proba: f64 = match args.next() {
        Some(value) => value.parse().unwrap(),
        None => 0.5,
    };

    let log_step_keep = f64::ln(10.0) / 100.0;

    let bernoulli_sum = BernoulliSum::new(|n| {
        if bsum_type != "binomial" {
            1.0 / (n as f64)
        } else {
            proba
        }
    });

    bernoulli_sum
        .enumerate()
        .take(limit)
        .skip(2)
        .group_by(|(x, _)| (f64::ln(*x as f64) / log_step_keep).floor())
        .into_iter()
        .map(|(_, it)| it.last().unwrap())
        .for_each(|(n, distrib)| {
            println!(
                "{}\t{}\t{}",
                n,
                distrib.norm_cdf_dinf(),
                distrib.norm_discrete_pmf_ninf_relative()
            )
        });
}
