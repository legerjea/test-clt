The main objective is to illustrate slow convergence under [CLT].

---

# Math context

## Preamble

Let $`Y_i\sim\mathcal B(p_i)`$ independantly distributed variables, with
$`(p_i)_i`$ a known sequence.

Let 

```math
X_n = \sum_{i=1}^n Y_i
```

With $`\mu_n=\operatorname{\mathbb E}X_n=\sum_{i=1}^n p_i`$, and with
$`\sigma_n^2=\operatorname{var}X_n=\sum_{i=1}^n p_i(1-p_i)`$.

If we have sufficiant hypothesis on $`(p_i)_i`$ we can demonstrate:

```math
\frac{X_n-\mu_n}{\sqrt{\sigma_n^2}} \overset{(d)}{\underset{n\to\infty}\rightarrow} \mathcal{N}(0,1)
```

Examples of sufficiant conditions:

 1. $`\exists p,\; \forall i,\; p_i=p`$. With this hypothesis, $`(Y_i)_i`$ are
   i.i.d. and this is a binomial distribution. The result is a direct
    application of the 
   [classical CLT][CCLT]
   (in particular, this is the [Moivre-Laplace Theorem][MLT]).
 2. $`\forall i,\; p_i=\frac1i`$. This is a harmonic Bernoulli sum. This can be demonstrated using the 
   [Lyapunov CLT][LCLT].

For the first case, the convergence is fast. For the second case, the
convergence is **very** slow. The idea of this project is to illustrate
numerically this.

[CLT]: https://en.wikipedia.org/wiki/Central_limit_theorem
[CCLT]: https://en.wikipedia.org/wiki/Central_limit_theorem#Classical_CLT
[LCLT]: https://en.wikipedia.org/wiki/Central_limit_theorem#Lyapunov_CLT
[MLT]: https://en.wikipedia.org/wiki/De_Moivre%E2%80%93Laplace_theorem

## Normality measure

### First criterion: infinite norm between cdf

The CLT convergence is a convergence _in distribution_, meaning it is a
convergence in cdf space. Let $`F_n`$ the `cdf` of
`\frac{X_n-\mu_n}{\sqrt{\sigma_n^2}}`. The CLT states:

```math
F_n \underset{n\to\infty}\rightarrow \Phi
```

So, the fist idea is to study:
```math
\left\|F_n-\Phi\right\|_\infty
```

### Second criterion: relative infinit norm between pmf

As the CLT is often used as a approximation to approximate the probabilities we often use:

```math
\mathbb h_{n,k}=\mathbb P(X_n=k) \approx 
\Phi\left(\frac{k+\frac12-\mu_n}{\sqrt{\sigma_n^2}}\right)
-
\Phi\left(\frac{k-\frac12-\mu_n}{\sqrt{\sigma_n^2}}\right)
```

Let introduce the pmf of the discretized normal distribution:
```math
g_{n,k} =
\Phi\left(\frac{k+\frac12-\mu_n}{\sqrt{\sigma_n^2}}\right)
-
\Phi\left(\frac{k-\frac12-\mu_n}{\sqrt{\sigma_n^2}}\right)
```

The approximation is good if the pmf $`k\mapsto h_{n,k}`$ can be approximated by $`k\mapsto g_{n,k}`$. So we
introduce the second criterion:

```math
\frac{\|h_n-g_n\|_\infty}{\|h_n\|_\infty}
```

## Maths tricks

We need close formula to compute probabilities.

Let $`\mathbb h_{n,k}=\mathbb P(X_n=k)`$, we have:

```math
\mathbb h_{n,k}=P(X_n=k)=
\mathbb P\left((Y_{n-1}=k-1\wedge X_n=1)\vee(Y_{n-1}=k\wedge X_n=0)\right)=
h_{n-1,k-1}p_n+h_{n-1,k}(1-p_n)
```

The main idea is to store only the non-zero values (i.e. for
$`k\in\{0,\cdots,n\}`$) of $`h_{n,k}`$ for a given $`n`$ as a vector $`h_n`$, this
vector represents the probability mass function. With $`h_n`$ we can compute
$`h_{n+1}`$.

Initialization: for $`n=0`$ we have a degenerated random variable fixed to
$`0`$, therefore $`h_0=(1)`$.

# Run the code

The code provide for different value of $`n`$ (regulary spaced in log-space) the
value of both criterion. For the binomial case:

```bash
target/release/test_clt 1000 binomial 0.5
```

The `1000` is the limit for $`n`$, and `0.5` is the value of $`p`$.

For the harmonic Bernoulli sum:

```bash
target/release/test_clt 1000 harmonic
```

# Result

## First criterion

![](img/c1.png)

## Second criterion

![](img/c2.png)
